package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class CarPosition {

    private CarId id;
    private double angle;
    private PiecePosition piecePosition;

    public CarId getId() {
        return id;
    }

    public double getAngle() {
        return angle;
    }

    public PiecePosition getPiecePosition() {
        return piecePosition;
    }
}
