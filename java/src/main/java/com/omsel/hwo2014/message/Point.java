package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class Point {

    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
