package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class JoinRace extends Join {

    public static final String TYPE = "joinRace";

    private final String trackName;
    private final int carCount;

    public JoinRace(String name, String key, String trackName, int carCount) {
        super(name, key);

        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    public String msgType() {
        return TYPE;
    }

    @Override
    public Object msgData() {
        return this;
    }
}
