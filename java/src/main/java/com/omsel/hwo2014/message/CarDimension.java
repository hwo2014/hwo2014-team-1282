package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class CarDimension {

    private double length;
    private double width;
    private double guideFlagPosition;

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }
}
