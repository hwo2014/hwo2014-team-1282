package com.omsel.hwo2014.message;

import com.google.gson.annotations.SerializedName;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class Piece {

    private double length;

    @SerializedName("switch")
    private boolean isSwitch;

    private int radius;
    private double angle;

    public double getLength() {
        return length;
    }

    public boolean isSwitch() {
        return isSwitch;
    }

    public int getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }
}
