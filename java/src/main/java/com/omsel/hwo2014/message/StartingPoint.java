package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class StartingPoint {

    private Point position;
    private double angle;

    public Point getPosition() {
        return position;
    }

    public double getAngle() {
        return angle;
    }
}
