package com.omsel.hwo2014.message;

public class Ping extends SendMsg {
    @Override
    public String msgType() {
        return "ping";
    }
}