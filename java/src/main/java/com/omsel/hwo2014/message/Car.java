package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class Car {

    private CarId id;
    private CarDimension dimensions;

    public CarId getId() {
        return id;
    }

    public CarDimension getDimensions() {
        return dimensions;
    }
}
