package com.omsel.hwo2014.message;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class Race {

    private Track track;
    private List<Car> cars;
    private RaceSession raceSession;

    public Track getTrack() {
        return track;
    }

    public List<Car> getCars() {
        return cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    @Override
    public String toString() {
        return "Race{" +
                "track=" + track +
                ", cars=" + cars.size() +
                ", raceSession=" + raceSession +
                '}';
    }
}
