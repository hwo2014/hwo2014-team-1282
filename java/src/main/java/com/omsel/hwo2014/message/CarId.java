package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 17.04.14
 */
public class CarId {

    public static final String TYPE = "yourCar";

    String name;
    String color;

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }
}
