package com.omsel.hwo2014.message;

public class MsgWrapper {
    private final String msgType;
    private final Object data;

    public MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }

    public String getMsgType() {
        return msgType;
    }

    public Object getData() {
        return data;
    }
}
