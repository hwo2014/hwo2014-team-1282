package com.omsel.hwo2014.message;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class GameInit {

    public static final String TYPE = "gameInit";

    private Race race;

    public Race getRace() {
        return race;
    }
}
