package com.omsel.hwo2014.message;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class GameTick {

    public static final String TYPE = "carPositions";

    private final List<CarPosition> carPositions;
    private final String gameId;
    private final long gameTick;

    public GameTick(List<CarPosition> carPositions, String gameId, long gameTick) {
        this.carPositions = carPositions;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

    public List<CarPosition> getCarPositions() {
        return carPositions;
    }

    public String getGameId() {
        return gameId;
    }

    public long getGameTick() {
        return gameTick;
    }
}
