package com.omsel.hwo2014.message;

public class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public Object msgData() {
        return value;
    }

    @Override
    public String msgType() {
        return "throttle";
    }
}