package com.omsel.hwo2014.message;

public class Join extends SendMsg {

    public static final String TYPE = "join";

    private final BotId botId;

    public Join(final String name, final String key) {
        this.botId = new BotId(name, key);
    }

    @Override
    public String msgType() {
        return TYPE;
    }

    @Override
    public Object msgData() {
        return botId;
    }
}
