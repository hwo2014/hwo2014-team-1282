package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class Lane {

    private int distanceFromCenter;
    private int index;

    public int getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public int getIndex() {
        return index;
    }
}
