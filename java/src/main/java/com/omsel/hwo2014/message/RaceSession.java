package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class RaceSession {

    private int laps;
    private long maxLapTimeMs;
    private boolean quickRace;

    public int getLaps() {
        return laps;
    }

    public long getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public boolean isQuickRace() {
        return quickRace;
    }

    @Override
    public String toString() {
        return "RaceSession{" +
                "laps=" + laps +
                ", maxLapTimeMs=" + maxLapTimeMs +
                ", quickRace=" + quickRace +
                '}';
    }
}
