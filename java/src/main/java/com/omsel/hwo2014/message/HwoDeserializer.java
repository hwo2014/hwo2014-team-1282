package com.omsel.hwo2014.message;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 17.04.14
 */
public class HwoDeserializer implements JsonDeserializer<MsgWrapper> {
    @Override
    public MsgWrapper deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        String type = jsonObject.get("msgType").getAsString();
        JsonElement data = jsonObject.get("data");
        Object dData;
        switch (type) {
            case CarId.TYPE:
                dData = context.deserialize(data, CarId.class);
                break;
            case GameInit.TYPE:
                dData = context.deserialize(data, GameInit.class);
                break;

            case GameTick.TYPE:
                List<CarPosition> carPositions = context.deserialize(data, new TypeToken<List<CarPosition>>(){}.getType());
                JsonElement gameId = jsonObject.get("gameId");
                JsonElement gameTick = jsonObject.get("gameTick");
                String gameIdStr = gameId != null ? gameId.getAsString() : null;
                long gameTickLong = gameTick != null ? gameTick.getAsLong() : 0;
                dData = new GameTick(carPositions, gameIdStr, gameTickLong);
                break;

            default:
                dData = data;
                break;
        }

        return new MsgWrapper(type, dData);
    }
}
