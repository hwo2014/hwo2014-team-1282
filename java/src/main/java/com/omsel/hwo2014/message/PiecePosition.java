package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class PiecePosition {

    private int pieceIndex;
    private double inPieceDistance;
    private LaneIndex lane;
    private int lap;

    public int getPieceIndex() {
        return pieceIndex;
    }

    public double getInPieceDistance() {
        return inPieceDistance;
    }

    public LaneIndex getLane() {
        return lane;
    }

    public int getLap() {
        return lap;
    }
}
