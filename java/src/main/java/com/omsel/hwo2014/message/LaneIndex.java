package com.omsel.hwo2014.message;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class LaneIndex {

    private int startLaneIndex;
    private int endLaneIndex;

    public int getStartLaneIndex() {
        return startLaneIndex;
    }

    public int getEndLaneIndex() {
        return endLaneIndex;
    }
}
