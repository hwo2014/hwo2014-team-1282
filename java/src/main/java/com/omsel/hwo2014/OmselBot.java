package com.omsel.hwo2014;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.omsel.hwo2014.message.*;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Markus Oppeneiger
 * Date: 18.04.14
 */
public class OmselBot {

    public static void main(String... args) throws IOException {
        /*String host = "testserver.helloworldopen.com";
        int port = 8091;
        String botName = "Omsel";
        String botKey = "RKmuMPI+wCY1XA";*/

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new OmselBot(reader, writer, new JoinRace(botName, botKey, "germany", 1));
    }

    private Gson gson;
    private PrintWriter writer;

    private Status status = Status.Joining;

    private Race race;
    private CarId myCar;
    private Map<Long,List<CarPosition>> carPositions = new HashMap<>();

    private SendMsg sendMsg;
    private Throttle throttle = new Throttle(1);
    private Ping ping = new Ping();

    public OmselBot(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MsgWrapper.class, new HwoDeserializer());
        gson = gsonBuilder.create();

        send(join);

        String line;
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            switch (msgFromServer.getMsgType()) {
                case Join.TYPE:
                {
                    break;
                }
                case JoinRace.TYPE:
                {
                    break;
                }
                case CarId.TYPE:
                {
                    myCar = (CarId) msgFromServer.getData();
                    break;
                }
                case GameInit.TYPE:
                {
                    GameInit gameInit = (GameInit) msgFromServer.getData();
                    race = gameInit.getRace();
                    System.out.println(race);
                    break;
                }
                case GameTick.TYPE:
                {
                    GameTick tick = (GameTick) msgFromServer.getData();
                    carPositions.put(tick.getGameTick(), tick.getCarPositions());
                    break;
                }
                case "gameEnd":
                    status = Status.Finished;
                    break;
                case "gameStart":
                    status = Status.Racing;
                    break;
                case "lapFinished":
                    break;
                case "finish":
                    break;
                case "tournamentEnd":
                    break;
                case "crash":
                    status = Status.Crashed;
                    System.out.println("crash");
                    break;
                case "spawn":
                    status = Status.Racing;
                    System.out.println("spawn");
                    break;

                default:
                    System.out.println("unknown: " + msgFromServer.getMsgType());
                    break;
            }

            update();
            send(sendMsg);
        }
    }

    private void update() {
        switch (status) {
            case Racing:
                throttle.setValue(0.45);
                sendMsg = throttle;
                break;
            default:
                sendMsg = ping;
                break;
        }
    }

    private void send(final SendMsg msg) {
        String x = msg.toJson();
        writer.println(x);
        writer.flush();
    }

    static enum Status {
        Joining,
        Racing,
        Finished,
        Crashed
    }
}
